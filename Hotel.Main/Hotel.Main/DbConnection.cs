﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Hotel.Main
{
    public class DbConnection
    {
        public MySqlConnection hotelConnection { get; private set; }

        public DbConnection()
        {
            hotelConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["hotelConnectionString"].ConnectionString);
        }
    }
}
