﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Hotel.ViewModels;

namespace Hotel.Services
{
    public class ReservationService : BaseService
    {
        public ReservationService()
        {

        }

        public int CreateReservation()
        {
            var createReservationCmdText = "INSERT INTO reservations (RecordDate) VALUES (@RecordDate);";
            var createReservationCmd = new MySqlCommand(createReservationCmdText, hotelConnection);

            createReservationCmd.Parameters.AddWithValue("@RecordDate", DateTime.Now);

            OpenConnecion();
            createReservationCmd.ExecuteNonQuery();
            CloseConnection();
            return (int)createReservationCmd.LastInsertedId;
        }

        public List<TempRoomAddViewModel> GetTempRooms(int reservationId)
        {
            List<TempRoomAddViewModel> tempRooms = new List<TempRoomAddViewModel>();
            var getTempRoomsCmdText = "SELECT * FROM temproomadding where ReservationId = @ReservationId";
            var getTempRoomsCmd = new MySqlCommand(getTempRoomsCmdText, hotelConnection);
            getTempRoomsCmd.Parameters.AddWithValue("@ReservationId", reservationId);

            OpenConnecion();

            var reader = getTempRoomsCmd.ExecuteReader();

            while (reader.Read())
            {
                tempRooms.Add(new TempRoomAddViewModel
                {
                    Id = reader.GetInt32("Id"),
                    ReservationId = reader.GetInt32("ReservationId"),
                    AccommodationTypeId = reader.GetInt32("AccommodationTypeId"),
                    RoomTypeId = reader.GetInt32("RoomTypeId"),
                    Quantity = reader.GetInt32("Quantity")
                });
            }

            CloseConnection();

            return tempRooms;
        }

        public int DeleteTempRoom(int id)
        {
            var deleteTempRoomCmdText = "DELETE FROM temproomadding where Id = @Id";
            var deleteTempRoomCmd = new MySqlCommand(deleteTempRoomCmdText, hotelConnection);

            //Parameters
            deleteTempRoomCmd.Parameters.AddWithValue("@Id", id);

            OpenConnecion();

            return deleteTempRoomCmd.ExecuteNonQuery();
        }

        public List<ReservationViewModel._ReservationRooms> ReservationRooms(ReservationViewModel reservation)
        {
            OpenConnecion();
            foreach (var room in reservation.ReservationRooms)
            {
                var roomPricePerNight = 0;
                var accommodationPricePerNight = 0;

                var roomTypePriceCmdText = "SELECT PricePerNight FROM RoomTypes where RoomTypeId = @RoomTypeId";
                var roomTypePriceCmd = new MySqlCommand(roomTypePriceCmdText, hotelConnection);
                roomTypePriceCmd.Parameters.AddWithValue("@RoomTypeId", (int)room.RoomType);
                roomPricePerNight = Convert.ToInt32(roomTypePriceCmd.ExecuteScalar());

                var accoTypePriceCmdText = "SELECT Price FROM accommodationTypes where Id = @Id";
                var accoTypePriceCmd = new MySqlCommand(accoTypePriceCmdText, hotelConnection);
                accoTypePriceCmd.Parameters.AddWithValue("@Id", (int)room.AccommodationType);
                accommodationPricePerNight = Convert.ToInt32(accoTypePriceCmd.ExecuteScalar());

                room.FullPrice = reservation.Nights * room.Quantity * (accommodationPricePerNight + roomPricePerNight);

            }
            CloseConnection();
            return reservation.ReservationRooms;
        }
    }
}
